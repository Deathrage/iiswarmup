# IIS Warmup

Dispatches HTTP or HTTPS request to specified URLs any time the application gets preloaded. Can perform windows authentication.

**Preparing application pool and site:**

1. Set pool's start mode to "Always running" - Application Pools > your pool > Advanced Settings > Start Mode
1. Set pool's idle timeout to 0 - Application Pools > your pool > Advanced Settings > Ide Time-out (minutes)
1. Enable site's preload - Sites > your site > Advanced settings > Preload Enabled

**Installing the module:**

1. Build/Publish the project using VS or other tools
1. Optional: move the dll to IIS folder or somewhere else
1. Registering new native module
   1. Go to Root
   1. Open Modules
   1. Click Configure Native Modules
   1. Click Register
   1. Select a name and path to the dll
   1. Click Ok
1. Adding the module
   1. Go to your site (to add to your site) or Root (to add to all sites)
   1. Open Modules
   1. Click Configure Native Modules
   1. Check your module (will have name from step 3)
   1. Click Ok
1. Configure the module in your site's web.config

**Configuration schema:**

```xml
<configSchema>
  <sectionSchema name="system.webServer/iiswarmup">
    <collection addElement="add">
      <attribute name="host" type="string" required="true"/>
      <attribute name="path" type="string" required="true"/>
      <attribute name="port" type="uint" required="false" defaultValue="80"/>
      <attribute name="method" type="string" required="false" defaultValue="GET"/>
      <attribute name="useSSL" type="bool" required="false" defaultValue="false"/>
    </collection>
  </sectionSchema>
</configSchema>

```

**Sample web.config:**

```xml

<configuration>
    <system.webServer>
        <iiswarmup>
            <add host="localhost" port="1000" path="/hello" />
            <add host="localhost" port="500" path="/somewhere-else" method="OPTIONS" />
        </iiswarmup>
    </system.webServer>
</configuration>


```

**Reading log**
You can find logs by the module in Event Viewer in Windows Logs > Application, event source will be IISWarmupModule
