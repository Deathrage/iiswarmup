#include "IISWarmupModule.h"
#include "nameof.h"

IISWarmupModule::IISWarmupModule(IHttpServer* pHttpServer)
{
    m_pHttpServer = pHttpServer;
    m_hEventLog = RegisterEventSource(NULL, TEXT(NAMEOF(IISWarmupModule)));
}

VOID IISWarmupModule::Terminate()
{
	delete this;
}

IISWarmupModule::~IISWarmupModule()
{
    // Test whether the handle for the Event Viewer is open.
    if (NULL != m_hEventLog)
    {
        // Close the handle to the Event Viewer.
        DeregisterEventSource(m_hEventLog);
        m_hEventLog = NULL;
    }
}
