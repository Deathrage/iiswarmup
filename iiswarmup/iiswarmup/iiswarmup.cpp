#include "IISWarmupModule.h"

HRESULT
__stdcall
RegisterModule(
    DWORD dwServerVersion,
    IHttpModuleRegistrationInfo* pModuleInfo,
    IHttpServer* pGlobalInfo
)
{
    UNREFERENCED_PARAMETER(dwServerVersion);
    
    IISWarmupModule* pModule = new IISWarmupModule(pGlobalInfo);

    if (pModule == NULL)
        return HRESULT_FROM_WIN32(ERROR_NOT_ENOUGH_MEMORY);

    pModuleInfo->SetGlobalNotifications(pModule, GL_APPLICATION_START);

    return S_OK;
}