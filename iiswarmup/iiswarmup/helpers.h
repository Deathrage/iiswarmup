#pragma once
#define _WINSOCKAPI_
#include <httpserv.h>
#include "config.h"

VOID CheckError(HRESULT result);

config CreateConfig(IHttpApplication* pHttpApplication, IAppHostAdminManager* pAdminManager);