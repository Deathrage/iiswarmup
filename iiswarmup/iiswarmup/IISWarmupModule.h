#pragma once
#define _WINSOCKAPI_
#include <httpserv.h>
#include "config.h"

class IISWarmupModule: public CGlobalModule
{
public:
    IISWarmupModule(IHttpServer* pHttpServer);
        
    GLOBAL_NOTIFICATION_STATUS OnGlobalApplicationStart(IN IHttpApplicationStartProvider* pProvider) override;

   	VOID Terminate() override;

    ~IISWarmupModule();

private:

    // Create a handle for the event viewer.
    HANDLE m_hEventLog;

    IHttpServer* m_pHttpServer;

    BOOL WriteEventViewerLog(LPCWSTR szNotification, WORD wType = EVENTLOG_INFORMATION_TYPE);

    VOID InvokeRequest(configItem item, LPCWSTR applicationId);
};

