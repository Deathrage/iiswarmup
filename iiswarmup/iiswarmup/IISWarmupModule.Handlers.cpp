#include "IISWarmupModule.h"
#include "helpers.h"

GLOBAL_NOTIFICATION_STATUS IISWarmupModule::OnGlobalApplicationStart(IN IHttpApplicationStartProvider* pProvider)
{
    IHttpApplication* pHttpApplication = pProvider->GetApplication();
    LPCWSTR applicationid = pHttpApplication->GetApplicationId();

    config config = CreateConfig(pHttpApplication, m_pHttpServer->GetAdminManager());

    std::vector<configItem> configItems = config.items;
    if (!configItems.empty())
        for (configItem& item : configItems)
            InvokeRequest(item, applicationid);
    
    return GL_NOTIFICATION_CONTINUE;
}
