#include "helpers.h"
#include "nameof.h"

VOID CheckError(HRESULT result) {
    if (S_OK == result)
        return;
    throw;
}

VARIANT GetElementProperty(LPCWSTR name, IAppHostElement* pElement) {
    IAppHostProperty* pTargetProperty = NULL;
    CheckError(pElement->GetPropertyByName(SysAllocString(name), &pTargetProperty));

    VARIANT value{};
    CheckError(pTargetProperty->get_Value(&value));

    return value;
}

config CreateConfig(IHttpApplication* pHttpApplication, IAppHostAdminManager* pAdminManager) {
    LPCWSTR confiPath = pHttpApplication->GetAppConfigPath();

    IAppHostElement* pElement = NULL;
    CheckError(pAdminManager->GetAdminSection(SysAllocString(L"system.webServer/iiswarmup"), SysAllocString(confiPath), &pElement));

    IAppHostElementCollection* pChildren = NULL;
    CheckError(pElement->get_Collection(&pChildren));

    DWORD count = 0;
    CheckError(pChildren->get_Count(&count));

    std::vector<configItem> vItems(0);

    for (int i = 0; i < (int)count; i++) {
        VARIANT vtItemIndex;
        vtItemIndex.vt = VT_I2;
        vtItemIndex.iVal = i;

        IAppHostElement* pChild = NULL;
        CheckError(pChildren->get_Item(vtItemIndex, &pChild));

        VARIANT host = GetElementProperty(TEXT(NAMEOF(host)), pChild);
        VARIANT path = GetElementProperty(TEXT(NAMEOF(path)), pChild);
        VARIANT port = GetElementProperty(TEXT(NAMEOF(port)), pChild);
        VARIANT method = GetElementProperty(TEXT(NAMEOF(method)), pChild);
        VARIANT useSSL = GetElementProperty(TEXT(NAMEOF(useSSL)), pChild);

        vItems.push_back({
            host.bstrVal,
            (USHORT)port.uintVal,
            method.bstrVal,
            path.bstrVal,
            useSSL.boolVal
        });
    }
    
    return {
        vItems
    };
}