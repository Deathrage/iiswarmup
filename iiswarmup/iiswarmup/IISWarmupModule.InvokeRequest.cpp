#include "IISWarmupModule.h"
#include <winhttp.h>
#include <windows.h>
#include <string>
#include <algorithm>
#include <thread>

VOID IISWarmupModule::InvokeRequest(configItem item, LPCWSTR applicationId)
{
    std::thread thread([item, applicationId, this]() {
        HINTERNET hSession = NULL;
        BOOL bAutoLogonLow = false;
        HINTERNET hConnect = NULL;
        HINTERNET hRequest = NULL;
        BOOL bResults = false;

        hSession = WinHttpOpen(L"WinHTTP Example/1.0",
            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
            WINHTTP_NO_PROXY_NAME,
            WINHTTP_NO_PROXY_BYPASS,
            0);

        if (hSession) {
            DWORD logonPolicy = WINHTTP_AUTOLOGON_SECURITY_LEVEL_LOW;
            bAutoLogonLow = WinHttpSetOption(
                hSession,
                WINHTTP_OPTION_AUTOLOGON_POLICY,
                (LPVOID*)&logonPolicy,
                sizeof(logonPolicy)
            );
        }

        if (bAutoLogonLow)
            hConnect = WinHttpConnect(hSession,
                item.host,
                item.port,
                0);

        std::wstring method;
        if (hConnect) {
            method = std::wstring(item.method);
            std::transform(method.begin(), method.end(), method.begin(), ::toupper);

            hRequest = WinHttpOpenRequest(hConnect,
                method.c_str(),
                item.path,
                NULL,
                WINHTTP_NO_REFERER,
                WINHTTP_DEFAULT_ACCEPT_TYPES,
                item.useSSL ? WINHTTP_FLAG_SECURE : 0);
        }

        if (hRequest)
            bResults = WinHttpSendRequest(hRequest,
                WINHTTP_NO_ADDITIONAL_HEADERS,
                0,
                WINHTTP_NO_REQUEST_DATA,
                0,
                0,
                0);

        if (bResults)
            bResults = WinHttpReceiveResponse(hRequest, NULL);

        std::wstring message;
        if (!bResults) {
            message = L"Error " + std::to_wstring(GetLastError()) + L" has occured during warming up of " + applicationId + L".";
            WriteEventViewerLog(message.c_str(), EVENTLOG_ERROR_TYPE);
        }
        else {
            message = L"Warmed up " + std::wstring(applicationId) + L" at " + method + L" " + (item.useSSL ? L"https://" : L"http://") + item.host + L":" + std::to_wstring(item.port) + item.path + L".";
            WriteEventViewerLog(message.c_str());
        }

        if (hRequest) WinHttpCloseHandle(hRequest);
        if (hConnect) WinHttpCloseHandle(hConnect);
        if (hSession) WinHttpCloseHandle(hSession);
    });

    thread.detach();
}
