#pragma once
#define _WINSOCKAPI_
#include <httpserv.h>
#include <vector>
#include <winhttp.h>


struct configItem {
	BSTR host;
	INTERNET_PORT port;
	BSTR method;
	BSTR path;
	BOOL useSSL;
};

struct config {
	std::vector<configItem> items;
};