#include "IISWarmupModule.h"

BOOL IISWarmupModule::WriteEventViewerLog(LPCWSTR szNotification, WORD wType)
{
    // Test whether the handle for the Event Viewer is open.
    if (NULL != m_hEventLog)
    {
        // Write any strings to the Event Viewer and return.
        return ReportEvent(
            m_hEventLog,
            wType,
            0, 
            0,
            NULL,
            1, 
            0,
            &szNotification,
            NULL);
    }
    return FALSE;
}

